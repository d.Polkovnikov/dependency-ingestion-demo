package com.dp9v.container;

import com.dp9v.container.annotation.Component;
import com.dp9v.container.annotation.Ignore;
import lombok.SneakyThrows;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class ObjectFactory {

    private final Map<String, Constructor<?>> definitions;
    private final Map<String, Object> context;

    @SneakyThrows
    public ObjectFactory(String packageName) {
        definitions = new HashMap<>();
        context = new HashMap<>();

        var reflection = new Reflections(packageName);
        for (Class<?> cls : reflection.getTypesAnnotatedWith(Component.class)) {
            var constructor = cls.getConstructors()[0];
            definitions.put(cls.getCanonicalName(), constructor);
        }
    }

    public Object getObject(Class<?> cls) {
        return createObject(cls.getCanonicalName());
    }

    @SneakyThrows
    private Object createObject(String className)  {
        var constructor = definitions.get(className);
        var constructorParameters = constructor.getParameters();
        Object[] parameters = new Object[constructorParameters.length];
        for (int i = 0; i < constructorParameters.length; i++) {
            Parameter constructorParameter = constructorParameters[i];
            String newObjectClassName = constructorParameter.getType().getCanonicalName();
            if (context.containsKey(newObjectClassName)) {
                parameters[i] = context.get(newObjectClassName);
            } else {
                parameters[i] = createObject(newObjectClassName);
            }
        }
        Object newObject = constructor.newInstance(parameters);
        context.put(newObject.getClass().getCanonicalName(), newObject);
        return newObject;
    }


    public Object createObject(Constructor<?> constructor, Method method) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        var params = constructor == null ? method.getParameters() : constructor.getParameters();
        Object[] parameters = new Object[params.length];
        for (int i = 0; i < params.length; i++) {
            var parameter = params[i];
            var name = parameter.getType().getCanonicalName();
            if (context.containsKey(name)) {
                parameters[i] = context.get(name);
            } else {
                parameters[i] = builders.get(name).get();
            }
        }
        Object newObject = null;
        if (constructor != null) {
            newObject = constructor.newInstance(parameters);
        } else {
            newObject = method.invoke(parameters);
        }
        context.put(newObject.getClass().getCanonicalName(), newObject);
        return newObject;
    }
}

