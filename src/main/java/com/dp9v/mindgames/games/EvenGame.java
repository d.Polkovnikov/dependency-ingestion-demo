package com.dp9v.mindgames.games;

import com.dp9v.container.annotation.Component;
import com.dp9v.mindgames.GameEngine;
import com.dp9v.mindgames.common.Randomizer;
import lombok.AllArgsConstructor;

import static com.dp9v.mindgames.GameEngine.ROUNDS_COUNT;

@Component
@AllArgsConstructor
public class EvenGame {
    public static final int NUMBER_LIMIT = 20;
    private static final String INTRO = "Answer 'yes' if the number is even, otherwise answer 'no'.";

    private final GameEngine engine;
    private final Randomizer randomizer;

    public void run() {
        engine.run(INTRO, generateQuestions());
    }

    private String[][] generateQuestions() {
        var result = new String[ROUNDS_COUNT][];
        for (int i = 0; i < ROUNDS_COUNT; i++) {
            result[i] = generateQuestion();
        }
        return result;
    }

    public String[] generateQuestion() {
        var generatedQuestions = randomizer.nextInt(NUMBER_LIMIT);
        return new String[]{
            Integer.toString(generatedQuestions),
            isEven(generatedQuestions) ? "yes" : "no"
        };
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }
}
