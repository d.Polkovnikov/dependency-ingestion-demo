package com.dp9v.mindgames.games;

import com.dp9v.container.annotation.Component;
import com.dp9v.mindgames.GameEngine;
import com.dp9v.mindgames.common.Randomizer;
import lombok.AllArgsConstructor;

import static com.dp9v.mindgames.GameEngine.ROUNDS_COUNT;

@Component
@AllArgsConstructor
public class CalcGame {
    public static final int NUMBER_LIMIT = 20;
    private static final String INTRO = "What is the result of the expression?";
    private static final char[] SYMBOLS = {'+', '-', '*'};

    private final GameEngine engine;
    private final Randomizer randomizer;

    public void run() {
        engine.run(INTRO, generateQuestions());
    }

    private String[][] generateQuestions() {
        var generatedQuestions = new String[ROUNDS_COUNT][];
        for (int i = 0; i < ROUNDS_COUNT; i++) {
            generatedQuestions[i] = generateQuestion();
        }
        return generatedQuestions;
    }

    private String[] generateQuestion() {
        var leftValue = randomizer.nextInt(NUMBER_LIMIT);
        var rightValue = randomizer.nextInt(NUMBER_LIMIT);
        var operator = randomizer.nextInt(SYMBOLS.length);
        return new String[]{
            "%d %c %d".formatted(leftValue, SYMBOLS[operator], rightValue),
            Integer.toString(calculate(leftValue, SYMBOLS[operator], rightValue))
        };
    }

    private int calculate(int left, char operation, int right) {
        return switch (operation) {
            case '+' -> left + right;
            case '-' -> left - right;
            case '*' -> left * right;
            default -> throw new RuntimeException("Unknown operator: " + SYMBOLS[operation]);
        };
    }
}
