package com.dp9v.mindgames.games;

import com.dp9v.container.annotation.Component;
import com.dp9v.mindgames.GameEngine;
import com.dp9v.mindgames.common.Randomizer;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PrimeGame {
    private final GameEngine engine;
    private final Randomizer randomizer;
}
