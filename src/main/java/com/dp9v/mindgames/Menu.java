package com.dp9v.mindgames;

import com.dp9v.container.annotation.Component;
import com.dp9v.mindgames.common.Cli;
import com.dp9v.mindgames.common.ConsoleReader;
import com.dp9v.mindgames.games.CalcGame;
import com.dp9v.mindgames.games.EvenGame;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class Menu {
    public static final String[] OPTIONS =
        {"Exit", "Greet", "Even", "Calc", "GCD", "Progression", "Prime"};

    private final CalcGame calc;
    private final EvenGame even;
    private final ConsoleReader reader;
    private final Cli cli;

    public void run() {
        System.out.println("Please enter the game number and press Enter.");
        printOptions();
        System.out.print("Your choice: ");
        var option = reader.nextLine();

        switch (option) {
            case "0":
                System.out.println("\nWelcome to the Brain Games!");
                break;
            case "1":
                System.out.println("\nWelcome to the Even Game!");
                cli.greet();
                break;
            case "3":
                calc.run();
                break;
            case "2":
                even.run();
                break;
            default:
                throw new RuntimeException("Invalid option!");
        }
    }

    public void printOptions() {
        for (int i = 1; i < OPTIONS.length; i++) {
            System.out.printf("%d - %s\n", i, OPTIONS[i]);
        }
        System.out.printf("%d - %s\n", 0, "Exit");
    }
}
