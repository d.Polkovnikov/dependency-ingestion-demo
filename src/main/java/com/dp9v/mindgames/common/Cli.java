package com.dp9v.mindgames.common;

import com.dp9v.container.annotation.Component;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class Cli {

    private final ConsoleReader reader;

    public String greet() {
        System.out.print("May I have your name? ");
        var userName = reader.nextLine();
        System.out.printf("Hello, %s!\n", userName);
        return userName;
    }
}
