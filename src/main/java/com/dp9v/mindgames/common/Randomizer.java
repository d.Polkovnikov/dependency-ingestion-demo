package com.dp9v.mindgames.common;

import com.dp9v.container.annotation.Component;
import com.dp9v.container.annotation.Ignore;

import java.util.Random;

@Component
public class Randomizer {

    @Ignore
    private Random random = new Random();

    public int nextInt(int limit) {
        return random.nextInt(0, limit);
    }

    public int nextInt(int leftLimit, int rightLimit) {
        return random.nextInt(leftLimit, rightLimit);
    }
}
