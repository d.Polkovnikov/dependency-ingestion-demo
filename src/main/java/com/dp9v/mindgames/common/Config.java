package com.dp9v.mindgames.common;

import java.util.Random;
import java.util.Scanner;

public class Config {

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }

    public Random random() {
        return new Random();
    }
}
