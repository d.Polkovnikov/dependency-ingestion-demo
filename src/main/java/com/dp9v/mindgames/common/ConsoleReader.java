package com.dp9v.mindgames.common;

import com.dp9v.container.annotation.Component;
import com.dp9v.container.annotation.Ignore;

import java.util.Scanner;

@Component
public class ConsoleReader {

    private final Scanner scanner;


    public String nextLine() {
        return scanner.nextLine();
    }
}
