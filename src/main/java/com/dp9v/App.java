package com.dp9v;

import com.dp9v.container.ObjectFactory;
import com.dp9v.mindgames.Menu;
import com.dp9v.mindgames.games.PrimeGame;

public class App {

    public static void main(String[] args) {
        var factory = new ObjectFactory("com.dp9v.mindgames");
        var game = (Menu) factory.getObject(Menu.class);
        var prime = factory.getObject(PrimeGame.class);
        game.run();
    }
}
